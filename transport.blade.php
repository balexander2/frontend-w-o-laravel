<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="lib/css/main-layout.css">
        <link rel="stylesheet" type="text/css" href="lib/css/font-awesome.min.css">
        <title>Transport operator</title>
    </head>
    <body>

    <header id="navbar">
        <div id="logo">
            <a href="/passenger"><img src="lib/img/orbita-post-logo.png" alt="Logo - Orbita Line"></a>
        </div>

        <nav>
            <a href="passenger.blade.php">Passenger</a>
            <a href="passenger.blade.php">Business</a>
            <a href="transport.blade.php">Transport operator</a>
            <a href="city.blade.php">City + PTA</a>
            <a href="city.blade.php">Funding organization</a>
            <a href="transport.blade.php">Technology</a>
            <a href="social.blade.php">About</a>
            <a href="social.blade.php">Press</a>
            <a href="social.blade.php">Contacts</a>
        </nav>

        <div class="linktoaction sub">
            <a href="/signup/">Make your buses smart</a>
        </div>
    </header>
    
    <section id="main">
        <header>
            <h1>Your personal bus is here. Forget the timetable, you decide when and where to go.</h1>
            <h2>Cheaper than your local public transit. Request a bus to any place in the city, any time.</h2>
        </header>

        <div id="jumbotron">
            <img src="lib/img/bus-bunching-London_Bus_route_55_Buses,_Clapton_Pond.jpg" alt="Bus bunching - Orbita Line">
        </div>

        <div class="linktoaction sub">
            <a href="/signup/">Make your buses smart</a>
        </div>

        <section id="benefits">
            <div>
                <h3>Serve up to 15% more passengers with the same buses.</h3>
                <p> And the same drivers.</p>
            </div>
            <div>
                <h3>Forget bus bunching.</h3>
                <p>Orbita Line optimizes bus movement, so that buses never bunch together.</p>
            </div>
            <div>
                <h3>Works with any bus.</h3>
                <p>Orbita Line provides a compatible navigation device that fits nicely into the bus dashboard.</p>
            </div>
            <div>
                <h3>Real smart mobility. </h3>
                <p>With Orbita Line any person in the city can summon a bus by smartphone, board it 15 minutes later and get to any bus stop in 20 minutes. It is so fast and affordable that can be used many times a day even by low-income persons.</p>
            </div>
        </section>

        <section id="awards" class="wallofimg">
            <h3>International awards and achievements</h3>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.png" alt="Venture Cup Sweden Väst - Absolute Winner - Orbita Line"></a>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.png" alt="Venture Cup Sweden Väst - People and Society - Orbita Line"></a>
            <a href="http://copernicus-masters.com/"><img src="lib/img/copernicus-masters-satapps-catapult-finalist-badge.png" alt="Copernicus Masters Finalist Badge- Orbita Line"></a>
            <a href="http://universityworldcup.com/"><img src="lib/img/university-startup-world-cup-finalist-badge-2015.png" alt="University Startup Finalist Badge - Orbita Line"></a>
            <a href="http://esnc.eu/"><img src="lib/img/esnc-finalist-badge-2015.png" alt="ESNC Finalist Badge - Orbita Line"></a>
            <a href="http://www.verizon.com/about/portal/powerful-answers/"><img src="lib/img/verizon-finalist-badge-2015.png" alt="Verizon Finalist Badge - Orbita Line"></a>
        </section>

        <section id="recognition" class="wallofimg">
            <h3>Internationally recognized</h3>
            <a href="http://www.aol.com/article/2015/09/24/verizon-announces-36-finalists-for-the-2015-powerful-answers-awa/21239515/?&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript"><img src="lib/img/aol-logo-black.png" alt="AOL - Orbita Line"></a>
            <a href="http://digital.di.se/artikel/regionvinnare-i-venture-cup"><img src="lib/img/di-digital.svg" alt="Dagens Industri Digital - Orbita Line"></a>
            <a href="http://www.bussmagasinet.se/2015/12/goteborgsforetag-prisat-for-uber-for-stadsbussar/"><img src="lib/img/buss-magasinet.jpeg" alt="Buss Magasinet - Orbita Line"></a>
            <a href="http://www.driva-eget.se/nyheter/affarer/har-ar-affarsideerna-som-tog-hem-storvinsten"><img src="lib/img/driva-eget.png" alt="Driva Eget - Orbita Line"></a>
            <a href="http://www.gp.se/ekonomi/1.2925288-goteborgsforetag-skapar-uber-for-bussar"><img src="lib/img/goteborgs-posten.png" alt="Göteborgs-Posten - Orbita Line"></a>
            <a href="http://sebgroup.com/sv/press/nyheter/regionvinnare-i-venture-cup"><img src="lib/img/seb.png" alt="SEB - Orbita Line"></a>
            <a href="http://www.ekonominyheter.se/nyheter/goteborgsforetag-skapar-uber-for-bussar,306321"><img src="lib/img/ekomi-nyheter.png" alt="EkonomiNyheter - Orbita Line"></a>
            <a href="http://www.breakit.se/artikel/2109/lovande-svenska-startups-far-dela-pa-over-en-halv-miljon-kronor"><img src="lib/img/breakit.png" alt="Breakit - Orbita Line"></a>
            <a href="http://www.techsite.io/p/199675"><img src="lib/img/techsite.svg" alt="TechSite - Orbita Line"></a>
        </section>

        <section id="partners" class="wallofimg">
            <h3>Partners</h3>
            <a href="http://www.esa.int/"><img src="lib/img/esa.bmp" alt="European Space Agency - Orbita Line"></a>
            <a href="http://almi.se/"><img src="lib/img/almi.png" alt="ALMI - Orbita Line"></a>
            <a href="http://www.gbgnfc.se/"><img src="lib/img/nfc.png" alt="NyföretagarCentrum Göteborgsregionen - Orbita Line"></a>
            <a href="http://www.connectvast.se/"><img src="lib/img/connect-vast.jpg" alt="Connect Väst - Orbita Line"></a>
        </section>

    </section>

    <aside class="linktoaction">
        <h3>Get a detailed white paper about the Orbita Line technology.</h3>
        <a href="/signup/">Make your buses smart</a>
    </aside>
    
    <footer>
        <hr>
        <nav>
            <a href="passenger.blade.php">Passenger</a>
            <a href="passenger.blade.php">Business</a>
            <a href="transport.blade.php">Transport operator</a>
            <a href="city.blade.php">City + PTA</a>
            <a href="city.blade.php">Funding organization</a>
            <a href="transport.blade.php">Technology</a>
            <a href="social.blade.php">About</a>
            <a href="social.blade.php">Press</a>
            <a href="social.blade.php">Contacts</a>
        </nav>

        <div id="contacts">
            <p>
                <img src="lib/img/eu-flag.png" alt="European Union Flag - Orbita Line"> 
                <img src="lib/img/swedish-flag.png" alt="Swedish Flag - Orbita Line">
                Orbita Line is the project of <a href="skanatek.se">Skanatek AB</a>, Sweden
            </p>
            <p><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:info@skanatek.se">info@skanatek.se</a></p>
            <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+46739040526">+46 73 904 0526</a></p>
            <p>Skanatek AB. Headquarters: Göteborg, Sweden. <a href="http://bolagsverket.se/">Organisation number</a>: 556932-6571. <a href="http://www.skatteverket.se/foretagorganisationer/moms.4.18e1b10334ebe8bc80002497.html">VAT number</a>: SE556932657101.</p>
        </div>

        <div id="social">
            <div id="socicon">
                <a href="mailto:email@orbitaline.com"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                <a href="blog.orbitaline.com"><i class="fa fa-rss" aria-hidden="true"></i></a>
                <a href="http://www.twitter.com/orbitaline"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="http://www.gplus.com/orbitaline"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="http://www.facebook.com/orbitaline"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://instagram.com/orbitaline"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://www.linkedin.com/company/orbita-line" class="socicon"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
            <p><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img src="lib/img/creative-commons.png" alt="Creative Commons - Orbita Line"></a> This site is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.</p>
            <p id="imgprovided">Images provided by: <a href="https://www.flickr.com/photos/christianhaugen/">Christian Haugen, <a href="https://www.flickr.com/photos/singaporebus/">Singapore Buses</a>, <a href="https://www.flickr.com/photos/sgtransport/">SG transport</a>, <a href="https://www.flickr.com/photos/ekilby/">Eric Kilby</a>, <a href="https://www.flickr.com/photos/itdp/">ITDP</a><p>
            <p>All the information on this site is published with reservation for errors and changes.</p>
        </div>
    </footer>
    <div id="next">
        <a href="city.blade.php"><p>Next</p></a>
    </div>

    </body>
</html>
